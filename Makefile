NAME     =auth
grpc:
	protoc --proto_path=. --proto_path=third_party --go_out=plugins=grpc:../../.. proto/rpc/*.proto
	protoc --proto_path=. --proto_path=third_party --go_out=plugins=grpc:../../.. proto/request/*.proto
	protoc --proto_path=. --proto_path=third_party --go_out=plugins=grpc:../../.. proto/response/*.proto

run-test:
	cd test;go test -run ''
