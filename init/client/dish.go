package client

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/request"
	"gitlab.com/foodstreets/newsfeed/protobuf/response"
)

type dish struct{}

type IDish interface {
	Get(req *request.Dish) (*response.Dish, error)
}

var DishClient dish

func (dish) Signup(req *request.Dish) (*response.Dish, error) {
	return &response.Dish{}, nil
	//	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//	defer cancel()
	//
	//	res, err := client.User().Signup(ctx, req)
	//	return res, err
}
