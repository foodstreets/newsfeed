package client

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/foodstreets/newsfeed/protobuf/request"
	"gitlab.com/foodstreets/newsfeed/protobuf/response"

	"gitlab.com/foodstreets/newsfeed/utils"

	"github.com/cenkalti/backoff"
	"github.com/golang/protobuf/ptypes/empty"
)

type eatery struct{}

var Eatery IEatery

type IEatery interface {
	Create(req *request.Eatery) (*response.Eatery, error)
	GetByID(req *request.Eatery) (*response.Eatery, error)
	DeleteByID(req *request.Eatery) (*empty.Empty, error)
	Update(req *request.Eatery) (*response.Eatery, error)
	SearchEatery(req *request.SearchEatery) (*response.Eateries, error)
	UpdateLocationIndexByIds(ids []int) error
}

func init() {
	Eatery = &eatery{}
}

func (eatery) Create(req *request.Eatery) (res *response.Eatery, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.Eatery().Create(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return
}

func (eatery) GetByID(req *request.Eatery) (res *response.Eatery, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.Eatery().GetByID(ctx, req)
		return err
	}
	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return
}

func (eatery) DeleteByID(req *request.Eatery) (res *empty.Empty, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.Eatery().DeleteByID(ctx, req)
		return err
	}

	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return
}

func (eatery) Update(req *request.Eatery) (res *response.Eatery, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.Eatery().Update(ctx, req)
		return err
	}
	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}

	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return
}

func (eatery) SearchEatery(req *request.SearchEatery) (res *response.Eateries, err error) {
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		res, err = client.Eatery().SearchEatery(ctx, req)
		fmt.Printf("err", err)
		return err
	}
	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}
	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return
}

func (eatery) UpdateLocationIndexByIds(ids []int) (err error) {
	req := &request.Ids{
		Ids: utils.TypeInt.ParseIntToInt64(ids),
	}
	retryable := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		_, err = client.Eatery().UpdateLocationIndexByIds(ctx, req)
		fmt.Printf("err", err)
		return err
	}
	notify := func(err error, t time.Duration) {
		log.Printf("error: %v happened at time: %v", err, t)
	}
	b := getExponentialBackOff()
	err = backoff.RetryNotify(retryable, b, notify)
	return

}
