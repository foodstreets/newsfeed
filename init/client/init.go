package client

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/foodstreets/newsfeed/protobuf/rpc"

	"github.com/cenkalti/backoff"
	"google.golang.org/grpc"
)

var (
	client    *Client
	oneClient sync.Once
)

const (
	InitialInterval = 300 * time.Millisecond
	MaxElapsedTime  = 3 * time.Minute
)

type Client struct {
	dish   rpc.DishClient
	eatery rpc.EateryClient
}

type GrpcConfig struct {
	Address string
}

func InitClient(conf GrpcConfig) {
	oneClient.Do(func() {
		fmt.Println("[NEWSFEED]: Ping")
		conn, err := grpc.Dial(conf.Address, grpc.WithInsecure())
		if err != nil {
			panic(err)
		}

		fmt.Println("[NEWSFEED]: Pong")

		client = &Client{
			dish:   rpc.NewDishClient(conn),
			eatery: rpc.NewEateryClient(conn),
		}
	})
}

func grpcClient() *Client {
	fmt.Println("clientGrpc", client)
	if client == nil {
		panic("Dis")
	}
	return client
}

func (c Client) Dish() rpc.DishClient {
	return c.dish
}

func (c Client) Eatery() rpc.EateryClient {
	return c.eatery
}

func getExponentialBackOff() *backoff.ExponentialBackOff {
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = InitialInterval
	b.MaxElapsedTime = MaxElapsedTime
	return b
}
