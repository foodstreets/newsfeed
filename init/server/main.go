package main

import (
	"fmt"
	"net"

	"gitlab.com/foodstreets/newsfeed/config"
	"gitlab.com/foodstreets/newsfeed/protobuf/rpc"

	"gitlab.com/foodstreets/newsfeed/api/handler"

	"gitlab.com/foodstreets/master/cmd"
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/newsfeed/interceptor"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	// declaration var
	// var name type = initValue
	dish   rpc.DishServer   = handler.NewDish()
	eatery rpc.EateryServer = handler.NewEatery()
)

func main() {
	cmd.Execute()
	run()
}

func run() {
	conf := config.Get()
	config := infra.ElasticConf{
		Host:  conf.Elastic.Host,  //"192.168.20.34",
		Port:  conf.Elastic.Port,  //9200,
		Index: conf.Elastic.Index, //"foodstreet",
	}
	infra.InitElastic(&config)

	listen, err := net.Listen("tcp", conf.GrpcNewsfeed.Url)
	if err != nil {
		fmt.Println("Disconnection server", err)
		return
	}

	fmt.Println("Start servering...")
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(
			interceptor.NewMultiUnaryServerInterceptor(
				interceptor.Recovery(),
			),
		),
	}

	server := grpc.NewServer(opts...)
	rpc.RegisterDishServer(server, dish)
	rpc.RegisterEateryServer(server, eatery)
	reflection.Register(server)

	err = server.Serve(listen)
	if err != nil {
		fmt.Println("Server not listen", err)
		return
	}
}
