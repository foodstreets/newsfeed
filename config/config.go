package config

import (
	"sync"

	"gitlab.com/foodstreets/master/config"
)

var (
	conf Config
	once sync.Once
)

type Config struct {
	config.Config
}

func load() {
	once.Do(func() {
		conf.Config = config.Load()
	})
}

func Load() Config {
	load()
	return conf
}

func Get() Config {
	load()
	return conf
}
