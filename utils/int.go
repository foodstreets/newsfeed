package utils

type typeInt struct{}

var TypeInt typeInt

func (typeInt) ParseIntToInt64(ids []int) (res []int64) {
	if len(ids) == 0 {
		return
	}

	res = make([]int64, len(ids))
	for i, id := range ids {
		res[i] = int64(id)
	}
	return
}

func (typeInt) ParseInt64ToInt(ids []int64) (res []int) {
	if len(ids) == 0 {
		return
	}

	res = make([]int, len(ids))
	for i, id := range ids {
		res[i] = int(id)
	}
	return
}
