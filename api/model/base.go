package model

type base struct {
}

type Model interface {
	GetIDStr() string
	GetType() string
	GetIndex() string
	GetVersion() string
}
