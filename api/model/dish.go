package model

import (
	"fmt"

	mModel "gitlab.com/foodstreets/master/model"
)

type Dish struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func (m Dish) GetIDStr() string {
	return fmt.Sprintf("dish_%d", m.Id)
}

func (m Dish) GetType() string {
	return fmt.Sprintf("_doc")
}

func (m Dish) GetIndex() string {
	return fmt.Sprintf("foodstreet")
}

func (m Dish) GetVersion() string {
	return fmt.Sprintf("0.0.1")
}

func NewBaseDish(input *mModel.Dish) Dish {
	ret := Dish{
		Id:   input.Id,
		Name: input.Name,
	}
	return ret
}
