package model

import (
	"fmt"

	mModel "gitlab.com/foodstreets/master/model"
)

type Eatery struct {
	Id      int    `json:"id"`
	Active  bool   `json:"active"`
	Address string `json:"address"`
	CityId  int    `json:"cityId"`
}

func (m Eatery) GetIDStr() string {
	return fmt.Sprintf("eatery_%d", m.Id)
}

func (m Eatery) GetType() string {
	return fmt.Sprintf("_doc")
}

func (m Eatery) GetIndex() string {
	return fmt.Sprintf("foodstreet")
}

func (m Eatery) GetVersion() string {
	return fmt.Sprintf("0.0.1")
}

func NewBaseEatery(input *mModel.Eatery) Eatery {
	ret := Eatery{
		Id:     input.Id,
		Active: input.Active,
		CityId: input.CityId,
	}
	return ret
}

type EateryId struct {
	Id int `json:"id"`
}

func (m EateryId) GetIDStr() string {
	return fmt.Sprintf("eatery_%d", m.Id)
}

func (m EateryId) GetType() string {
	return fmt.Sprintf("_doc")
}

func (m EateryId) GetIndex() string {
	return fmt.Sprintf("foodstreet")
}

func (m EateryId) GetVersion() string {
	return fmt.Sprintf("0.0.1")
}

func NewEateryId(id int) EateryId {
	return EateryId{
		Id: id,
	}
}

type Location struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type EateryLocation struct {
	Id       int      `json:"id"`
	Location Location `json:"location"`
}

func (m EateryLocation) GetIDStr() string {
	return fmt.Sprintf("eatery_%d", m.Id)
}

func (m EateryLocation) GetType() string {
	return fmt.Sprintf("_doc")
}

func (m EateryLocation) GetIndex() string {
	return fmt.Sprintf("foodstreet")
}

func (m EateryLocation) GetVersion() string {
	return fmt.Sprintf("0.0.1")
}

func NewEateryLocation(id int, lat, lng float64) EateryLocation {
	return EateryLocation{
		Id: id,
		Location: Location{
			Lat: lat,
			Lng: lng,
		},
	}
}
