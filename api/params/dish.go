package params

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/request"
)

type DishParam struct {
	Id   int
	Name string
}

type dish struct{}

var DishNewParam dish

func (dish) ParseToDishParam(req *request.Dish) DishParam {
	return DishParam{
		Id:   int(req.Id),
		Name: req.Name,
	}
}
