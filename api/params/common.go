package params

type common struct{}

var CommonNewParam common

func (common) ParseIDToInt(id int64) int {
	return int(id)
}
