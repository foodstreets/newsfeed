package entity

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"

	"gitlab.com/foodstreets/newsfeed/api/repo"

	"gitlab.com/foodstreets/newsfeed/api/model"

	masterModel "gitlab.com/foodstreets/master/model"

	"gitlab.com/foodstreets/master/orm"
)

var (
	regexNewEateryId = regexp.MustCompile(`[0-9]+`)
)

type eatery struct{}

var EateryEntity IEatery

type IEatery interface {
	Create(id int) error
	GetByID(id int) (model.Eatery, error)
	DeleteByID(id int) error
	Update(model.Eatery) (model.Eatery, error)
	SearchEatery(model.Eatery) (int64, []model.Eatery, error)
	UpdateLocationIndexByIds(ids []int) error
}

func init() {
	EateryEntity = &eatery{}
}

func (e eatery) Create(id int) (err error) {
	eatery, err := orm.Eatery.GetByID(id)
	if err != nil {
		return
	}
	if eatery == nil {
		fmt.Println("Can't get eatery")
		return nil
	}

	modelEatery := model.NewBaseEatery(eatery)
	err = repo.Eatery().Create(modelEatery)
	return
}

func (e eatery) GetByID(id int) (model.Eatery, error) {
	item := model.Eatery{
		Id: id,
	}

	res, err := repo.Eatery().GetByID(item)
	if err != nil {
		return model.Eatery{}, err
	}
	if res == nil {
		return model.Eatery{}, nil
	}

	var eatery model.Eatery
	err = json.Unmarshal(res.Source, &eatery)
	return eatery, err
}

func (e eatery) DeleteByID(id int) error {
	item := model.Eatery{
		Id: id,
	}
	err := repo.Eatery().DeleteByID(item)
	return err
}

func (e eatery) Update(item model.Eatery) (model.Eatery, error) {
	res, err := repo.Eatery().Update(item)
	if err != nil {
		return model.Eatery{}, err
	}
	if res == nil {
		return model.Eatery{}, nil
	}

	data, err := repo.Eatery().GetByID(item)
	if err != nil {
		return model.Eatery{}, err
	}
	if data == nil {
		return model.Eatery{}, nil
	}

	var eatery model.Eatery
	err = json.Unmarshal(data.Source, &eatery)
	return eatery, err
}

func (e eatery) UpdateLocationIndexByIds(eateryIds []int) (err error) {
	eateryAddresss, err := orm.EateryAddress.GetByEateryIds(eateryIds)
	fmt.Println(err)
	if err != nil {
		return
	}
	if len(eateryAddresss) == 0 {
		return
	}

	mapEateryIds := make(map[int]masterModel.EateryAddress, len(eateryAddresss))
	for _, eateryAddress := range eateryAddresss {
		mapEateryIds[eateryAddress.EateryId] = eateryAddress
	}

	results, err := repo.Eatery().GetByIDs(eateryIds)
	if err != nil {
		return
	}

	listEateryExist := []masterModel.EateryAddress{}
	for _, doc := range results.Docs {
		newId := regexNewEateryId.FindStringSubmatch(doc.Id)
		eateryId, _ := strconv.Atoi(newId[0])
		if eatery, exist := mapEateryIds[eateryId]; exist {
			listEateryExist = append(listEateryExist, eatery)
		}
	}

	modelEateryLocation := make([]model.Model, len(listEateryExist))
	for i, eatery := range listEateryExist {
		modelEateryLocation[i] = model.NewEateryLocation(eatery.EateryId, eatery.Lat, eatery.Lng)
	}

	err = repo.Eatery().BulkUpdateLocation(modelEateryLocation)
	if err != nil {
		return
	}

	return
}

func (e eatery) SearchEatery(item model.Eatery) (total int64, eateries []model.Eatery, err error) {
	//	boolQuery := &elastic.BoolQuery{}
	//	typeTermQuery := elastic.NewTermQuery("active", false)
	//	boolQuery.Must(typeTermQuery)

	//	matchPharseQuery := elastic.NewMatchQuery("name", item.Name)
	//	boolQuery.Must(matchPharseQuery)
	//
	//	sorts := []elastic.Sorter{}
	//	total, eateries, err = repo.Eatery().Search(0, 20, boolQuery, sorts)
	return
}
