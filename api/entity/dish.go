package entity

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/response"

	"gitlab.com/foodstreets/newsfeed/api/params"
)

type dish struct{}

type IDish interface {
	Get(param params.DishParam) (*response.Dish, error)
	Create(id int) error
}

var DishEntity dish

func (d dish) Get(param params.DishParam) (*response.Dish, error) {
	return &response.Dish{
		Id:   1,
		Name: "",
	}, nil
}
