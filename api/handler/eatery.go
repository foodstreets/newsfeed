package handler

import (
	"context"

	"gitlab.com/foodstreets/newsfeed/api/model"
	"gitlab.com/foodstreets/newsfeed/protobuf/request"
	"gitlab.com/foodstreets/newsfeed/protobuf/response"
	"gitlab.com/foodstreets/newsfeed/protobuf/rpc"

	"gitlab.com/foodstreets/newsfeed/utils"

	"gitlab.com/foodstreets/newsfeed/api/view"

	"gitlab.com/foodstreets/newsfeed/api/entity"

	"github.com/golang/protobuf/ptypes/empty"
)

type eatery struct{}

var _ rpc.EateryServer = &eatery{}

func NewEatery() *eatery {
	return &eatery{}
}

func (e eatery) Create(ctx context.Context, req *request.Eatery) (res *response.Eatery, err error) {
	id := int(req.Id)
	err = entity.EateryEntity.Create(id)
	return &response.Eatery{}, err
}

func (e eatery) GetByID(ctx context.Context, req *request.Eatery) (*response.Eatery, error) {
	id := int(req.Id)
	res := &response.Eatery{}
	eatery, err := entity.EateryEntity.GetByID(id)
	if err != nil {
		return res, err
	}
	if eatery.Id == 0 {
		return &response.Eatery{}, nil
	}

	return view.EateryView.PopulateEatery(eatery), nil
}

func (e eatery) DeleteByID(ctx context.Context, req *request.Eatery) (*empty.Empty, error) {
	ret := &empty.Empty{}
	id := int(req.Id)
	err := entity.EateryEntity.DeleteByID(id)
	return ret, err
}

func (e eatery) Update(ctx context.Context, req *request.Eatery) (res *response.Eatery, err error) {
	model := ParseReqToModel(req)
	eatery, err := entity.EateryEntity.Update(model)
	if err != nil {
		return &response.Eatery{}, err
	}

	res = ParseModelToResponse(eatery)
	return
}

func (e eatery) SearchEatery(ctx context.Context, req *request.SearchEatery) (eateries *response.Eateries, err error) {
	model := ParseSearchEateryToModel(req)
	total, res, err := entity.EateryEntity.SearchEatery(model)
	if err != nil {
		return &response.Eateries{}, err
	}

	eateries = view.EateryView.PopulateEateryIds(res, total)
	return
}

func ParseReqToModel(req *request.Eatery) model.Eatery {
	return model.Eatery{
		Id:      int(req.Id),
		Address: req.Address,
		Active:  req.Active,
	}
}

func ParseSearchEateryToModel(req *request.SearchEatery) model.Eatery {
	return model.Eatery{
		Address: req.Address,
		Active:  req.Active,
	}
}

func ParseModelToResponse(model model.Eatery) *response.Eatery {
	return &response.Eatery{
		Id:      int64(model.Id),
		Address: model.Address,
		Active:  model.Active,
	}
}

func (e eatery) UpdateLocationIndexByIds(ctx context.Context, req *request.Ids) (*empty.Empty, error) {
	ret := &empty.Empty{}
	ids := utils.TypeInt.ParseInt64ToInt(req.Ids)
	err := entity.EateryEntity.UpdateLocationIndexByIds(ids)
	return ret, err
}
