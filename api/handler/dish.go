package handler

import (
	"context"

	"gitlab.com/foodstreets/newsfeed/api/params"
	"gitlab.com/foodstreets/newsfeed/protobuf/request"
	"gitlab.com/foodstreets/newsfeed/protobuf/response"
	"gitlab.com/foodstreets/newsfeed/protobuf/rpc"

	"gitlab.com/foodstreets/newsfeed/api/view"

	"gitlab.com/foodstreets/newsfeed/api/entity"
)

type dish struct{}

var _ rpc.DishServer = &dish{}

func NewDish() *dish {
	return &dish{}
}

func (d dish) Get(ctx context.Context, req *request.Dish) (*response.Dish, error) {
	pDish := params.DishNewParam.ParseToDishParam(req)
	dish, err := entity.DishEntity.Get(pDish)
	if err != nil {
		return nil, err
	}

	return view.DishView.PopulateDish(dish), nil
}
