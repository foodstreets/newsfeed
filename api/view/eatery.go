package view

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/response"

	"gitlab.com/foodstreets/newsfeed/api/model"
)

type eatery struct{}

var EateryView eatery

func (e eatery) PopulateEatery(res model.Eatery) *response.Eatery {
	return &response.Eatery{
		Id:      int64(res.Id),
		Active:  res.Active,
		Address: res.Address,
	}
}

func (e eatery) PopulateEateryIds(res []model.Eatery, total int64) *response.Eateries {
	if len(res) == 0 {
		return &response.Eateries{}
	}

	eateryIds := make([]int64, len(res))
	for i, eatery := range res {
		eateryIds[i] = int64(eatery.Id)
	}

	return &response.Eateries{
		EateryIds: eateryIds,
		Total:     total,
	}
}
