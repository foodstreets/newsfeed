package view

import (
	"gitlab.com/foodstreets/newsfeed/protobuf/response"
)

type dish struct{}

var DishView dish

func (d dish) PopulateDish(res *response.Dish) *response.Dish {
	return &response.Dish{
		Id:   res.Id,
		Name: res.Name,
	}
}
