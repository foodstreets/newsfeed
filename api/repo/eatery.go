package repo

import (
	"reflect"

	"gitlab.com/foodstreets/newsfeed/api/model"

	"github.com/olivere/elastic/v7"
)

type eatery struct {
	base
}

//var Eatery eatery

func Eatery() eatery {
	return eatery{}
}

func (e eatery) Create(eatery model.Eatery) error {
	return e.create(eatery)
}

func (e eatery) GetByID(eatery model.Eatery) (*elastic.GetResult, error) {
	res, err := e.getByID(eatery)
	return res, err
}

func (e eatery) GetByIDs(ids []int) (res *elastic.MgetResponse, err error) {
	items := make([]model.Model, len(ids))
	for i, id := range ids {
		items[i] = model.NewEateryId(id)
	}

	res, err = e.getByIds(items)
	return
}

func (e eatery) DeleteByID(eatery model.Eatery) error {
	return e.deleteByID(eatery)
}

func (e eatery) BulkUpdateLocation(eateries []model.Model) error {
	return e.bulkUpdate(eateries)
}

func (e eatery) Update(eatery model.Eatery) (res *elastic.UpdateResponse, err error) {
	res, err = e.update(eatery)
	return
}

func (e eatery) Search(from, limit int, query elastic.Query, sorts []elastic.Sorter) (total int64, eateries []model.Eatery, err error) {
	resp, err := e.search(from, limit, query, sorts)
	if err != nil {
		return
	}
	// Response eateries
	eateries = make([]model.Eatery, resp.TotalHits())
	var item model.Eatery
	for i, item := range resp.Each(reflect.TypeOf(item)) {
		if ret, ok := item.(model.Eatery); ok {
			//eateries = append(eateries, ret)
			eateries[i] = ret
		}
	}
	return resp.TotalHits(), eateries, nil
}
