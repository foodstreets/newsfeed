package repo

import "gitlab.com/foodstreets/newsfeed/api/model"

type dish struct {
	base
}

var Dish dish

func (d dish) Create(dish model.Dish) error {
	return d.create(dish)
}
