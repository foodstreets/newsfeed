package repo

import (
	"context"

	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/newsfeed/api/model"

	"github.com/olivere/elastic/v7"
)

const (
	BaseIndex = "foodstreet"
)

type base struct {
}

func (base) create(item model.Model) (err error) {
	_, err = infra.Elastic().Index().
		Index(item.GetIndex()).
		Type(item.GetType()).
		Id(item.GetIDStr()).
		BodyJson(item).
		Do(context.Background())
	return
}

func (base) getByID(item model.Model) (res *elastic.GetResult, err error) {
	res, err = infra.Elastic().Get().
		Index(item.GetIndex()).
		Type(item.GetType()).
		Id(item.GetIDStr()).
		Do(context.Background())
	return
}

func (base) update(item model.Model) (res *elastic.UpdateResponse, err error) {
	res, err = infra.Elastic().Update().
		Index(item.GetIndex()).
		Type(item.GetType()).
		Id(item.GetIDStr()).
		RetryOnConflict(5).
		Doc(item).
		Do(context.Background())
	return
}

func (base) deleteByID(item model.Model) (err error) {
	_, err = infra.Elastic().Delete().
		Index(item.GetIndex()).
		Type(item.GetType()).
		Id(item.GetIDStr()).
		Do(context.Background())
	return
}

func (base) search(from, limit int, query elastic.Query, sorts []elastic.Sorter) (*elastic.SearchResult, error) {
	eQuery := infra.Elastic().
		Search(BaseIndex).
		Query(query)
	if len(sorts) > 0 {
		eQuery = eQuery.SortBy(sorts...)
	}
	results, err := eQuery.
		From(from).
		Size(limit).
		Index(BaseIndex).
		Do(context.Background())
	return results, err
}

func (base) bulkDelete(items []model.Model) error {
	if len(items) == 0 {
		return nil
	}
	bulk := infra.Elastic().Bulk()
	for _, item := range items {
		request := elastic.NewBulkDeleteRequest().
			Index(item.GetIndex()).
			Type(item.GetType()).
			Id(item.GetIDStr())
		bulk = bulk.Add(request)
	}
	_, err := bulk.Do(context.Background())
	return err
}

func (base) bulkCreate(items []model.Model) error {
	if len(items) == 0 {
		return nil
	}
	bulk := infra.Elastic().Bulk()
	for _, item := range items {
		request := elastic.NewBulkIndexRequest().
			Index(item.GetIndex()).
			Type(item.GetType()).
			Id(item.GetIDStr()).
			Doc(item)
		bulk = bulk.Add(request)
	}
	_, err := bulk.Do(context.Background())
	return err
}
func (base) getByIds(items []model.Model) (resp *elastic.MgetResponse, err error) {
	if len(items) == 0 {
		return nil, nil
	}
	bulk := infra.Elastic().Mget()
	for _, item := range items {
		request := elastic.NewMultiGetItem().
			Index(item.GetIndex()).
			Type(item.GetType()).
			Id(item.GetIDStr())
		bulk = bulk.Add(request)
	}

	resp, err = bulk.Do(context.Background())

	return resp, err
}
func (base) bulkUpdate(items []model.Model) error {
	if len(items) == 0 {
		return nil
	}
	bulk := infra.Elastic().Bulk()
	for _, item := range items {
		request := elastic.NewBulkUpdateRequest().
			Index(item.GetIndex()).
			Type(item.GetType()).
			Id(item.GetIDStr()).
			Doc(item)
		bulk = bulk.Add(request)
	}
	_, err := bulk.Do(context.Background())
	return err
}
